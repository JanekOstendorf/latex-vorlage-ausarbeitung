\chapter{Aufbau der Vorlage} % (fold)
\label{cha:aufbau-der-vorlage}

Die Vorlage ist so aufgebaut, dass der eigentliche Inhalt und die Stilvorgaben der Vorlage voneinander getrennt sind.
Die Vorlage baut sich aus diesen Dateien und Ordnern auf:

\begin{itemize}
    \item Die Hauptdatei dieser Vorlage ist \textbf{\texttt{ausarbeitung.tex}}. 
    Die Datei kann natürlich auch umbenannt werden.
    Von dieser Datei aus werden alle weiteren Dateien geladen.
    Sie selbst enthält aber nur wenige Einstellungen und Definitionen und muss somit nur selten angepasst werden. 

    \item Im \textbf{Ordner \texttt{include}} befinden sich alle Stilvorlagen und Definitionen aus der eigentlichen Vorlage. 
    Alles was hier definiert wird, ist unabhängig vom Inhalt des Dokuments.

    \item Im \textbf{Ordner \texttt{content}} befindet sich der Inhalt des Dokuments. 
    Dazu zählen auch Metainformationen wie der Titel und Autor, Glossar, Index.
    Außerdem findet sich hier noch die Datei \texttt{\_preamble.tex}, in der noch Definitionen und Anpassungen (\zB{} eigene Makros) für dieses spezielle Dokument gemacht werden können, die die Vorlage erweitern oder überschreiben.

    \item Im \textbf{Ordner \texttt{bib}} können Bibliographiedateien und Quellen abgelegt werden.
\end{itemize}

Die Vorlagen im Ordner \texttt{include} -- das eigentliche Herzstück der Vorlage -- ist in viele Dateien aufgeteilt.
Viele der Dateien müssen für die meisten Hausarbeiten oder Ausarbeitungen wahrscheinlich überhaupt nicht angepasst werden.
Sie sollten für die allermeisten Fälle funktionieren.

Wenn einzelne Bereiche der Vorlage nicht gefallen, können sie natürlich entweder direkt in der Datei im \texttt{include}-Ordner geändert werden, oder man überschreibt die entsprechenden Passagen der Vorlage in der Datei \texttt{\_preamble.tex}.
Letzteres hat den Vorteil, dass die Vorlage an sich sozusagen \enquote{sauber} bleibt.
Man könnte sie also \bspw{} später für eine neue Hausarbeit kopieren und muss nur im Ordner \texttt{content} Änderungen machen, weil der Rest allgemeingültig bleibt.

Im Folgenden gehe ich auf die einzelnen Bestandteile der Vorlage ein.
Zunächst auf die Hauptdatei, von der aus alle anderen Dateien geladen werden. 
Anschließend auf die verschiedenen Dateien im \texttt{include}-Ordner -- sowohl die Hauptmodule direkt im \texttt{include}-Ordner als auch auf die Add-On-Module, die die Vorlage um ein paar weitere Funktionen erweitern können.

\section{Hauptdatei}
\label{sec:hauptdatei}

Die Hauptdatei \texttt{ausarbeitung.tex} ist der Einstiegspunkt für \LaTeX{}.
Dies ist die Datei, die \LaTeX{} kompiliert und die alle weiteren Vorlagendateien und Inhaltsdateien lädt.

Zunächst vorweg: Ja, die Datei kann gerne auch umbenannt werden.
Der Name der Hauptdatei tut nichts zur Sache.
Wenn die Hauptdatei umbenannt wird, ändert sich \zB{} auch der Name der fertigen PDF-Datei.

Die Hauptdatei hat mehrere Aufgaben:

\minisec{Dokumentenklasse}

In der Hauptdatei werden die Dokumentenklasse und die Klassenoptionen festgelegt.
Diese werden ganz am Anfang der Datei gesetzt und sind direkt in der Hauptdatei auch schon kommentiert und beschrieben.

Diese Vorlage beruht auf dem \emph{KOMA-Script} und seinen Paketen und Klassen.
KOMA-Script erhält seinen Namen vom Entwickler \underline{Ma}rkus \underline{Ko}hm, wird aber auch von Frank Neukam und Axel Kielhorn entwickelt.
KOMA-Script konzentriert sich auf zwei Bereiche im Vergleich zum \LaTeX{}-Standard: Typografie, also die Gestaltung von Druckwerken, nach europäischen Standards und die Flexibilisierung der Standard-\LaTeX{}-Klassen.
Wie genau das funktioniert, ist in der sehr ausführlichen Dokumentation zu KOMA-Script gut beschrieben.
\autocite{Kohm2021}

Diese Vorlage verwendet die Dokumentenklasse \texttt{scrbook}, die ursprünglich für längere Arbeiten und Bücher gedacht ist.
Diese Klasse kann aber auch für kürzere Arbeiten genutzt werden.
Gegebenenfalls sind noch einzelne Anpassungen sinnvoll, wenn es sich um eine kurze Arbeit handelt (siehe \zB{} \cref{subsec:sectionlike-chapters}).

Zu Beginn der Datei werden ebenfalls die Sprachen festgelegt, die im Dokument vorkommen.
Für Deutsch sollte immer \texttt{ngerman} verwendet werden, um die neue Rechtschreibung zu erhalten.
Über die Sprache wird unter anderem gesteuert, wie die Silbentrennung erfolgt und wie bestimmte Formatierungen erfolgen, \zB{} \enquote{Anführungszeichen} im Text und bei Zahlen das Dezimaltrennzeichen oder das Tausendertrennzeichen (\num{10234,56} vs. \num[locale=UK,group-separator={,}]{10234,56}).

Außerdem wird festgelegt, ob es sich um einen Entwurf oder um das finale Dokument handelt. 
Im Entwurfsmodus werden für überlange Zeilen schwarze Kästen eingeblendet, um diese Zeilen schneller finden zu können.
\autocite[165]{Kohm2021}

\minisec{Module}

Die meisten Einstellungen der \LaTeX{}-Vorlage sind in einzelne Module aufgeteilt, die von der Hauptdatei aus geladen werden.
Diese Module sind einzelne \LaTeX{}-Dateien, die im Ordner \texttt{include} liegen.

Die Module sind aufgeteilt in \emph{Kernmodule} und \emph{Add-On-Module}.
Die einzelnen Module sind weiter in \vref{sec:kernmodule} und \vref{sec:add-on-module} beschrieben.
Die Kernmodule sind unbedingt notwendig, damit diese Vorlage funktioniert.
Sie enthalten die grundlegenden Einstellungen für das Dokument.
Die Module können dann je nach belieben für das eigene Dokument angepasst werden, \zB{} die Datei \texttt{typography.tex}, um die Schriftart zu ändern.

Die Module bauen teilweise aufeinander auf und sind abhängig von einander.
Die Reihenfolge, in der die Module geladen werden, sollte deswegen nicht verändert werden.

\section{Kern-Module}
\label{sec:kernmodule}

\section{Add-On-Module}
\label{sec:add-on-module}

\subsection{Kapitel wie Abschnitte (sectionlike-chapters.tex)}
\label{subsec:sectionlike-chapters}

% chapter aufbau-der-vorlage (end)