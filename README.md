# LATEX-Vorlage

Eine allgemeine Vorlage für Haus-, Projekt- und Abschlussarbeiten

---

PDF-Download: [![build status](https://gitlab.com/JanekOstendorf/latex-vorlage-ausarbeitung/badges/master/pipeline.svg)](https://gitlab.com/JanekOstendorf/latex-vorlage-ausarbeitung/-/jobs/artifacts/master/raw/ausarbeitung.pdf?job=compile_pdf) - Entwurf: [![build status](https://gitlab.com/JanekOstendorf/latex-vorlage-ausarbeitung/badges/entwurf/pipeline.svg)](https://gitlab.com/JanekOstendorf/latex-vorlage-ausarbeitung/-/jobs/artifacts/entwurf/raw/ausarbeitung.pdf?job=compile_pdf)
